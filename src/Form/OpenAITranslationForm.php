<?php

namespace Drupal\openai_translation\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Drupal\openai_translation\Service\OpenAITranslationService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * OpenAI Translation Toolbox form.
 */
class OpenAITranslationForm extends FormBase {

  /**
   * The OpenAI translation service.
   *
   * @var Drupal\openai_translation\Service\OpenAITranslationService
   */
  protected OpenAITranslationService $translationService;


  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs a new OpenAITranslationForm.
   *
   * @param \Drupal\openai_translation\Service\OpenAITranslationService $translation_service
   *   The OpenAI translation service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(OpenAITranslationService $translation_service, ConfigFactoryInterface $config_factory, LanguageManagerInterface $language_manager) {
    $this->translationService = $translation_service;
    $this->configFactory = $config_factory;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openai_translation.openai_translation_service'),
      $container->get('config.factory'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openai_translation_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('openai_translation.settings');

    // Check if the config contains openai_api_service and enabled_languages.
    // If not redirect to the settings page.
    if (!$config->get('openai_api_service') || !$config->get('enabled_languages')) {
      $form['#markup'] = $this->t('Please configure the OpenAI API service and enabled languages in the <a href=":url">OpenAI Translation Toolbox settings</a>.', [
        ':url' => Url::fromRoute('openai_translation.settings')->toString(),
      ]);
      return $form;
    }

    $enabled_languages = $config->get('enabled_languages');
    $system_languages = $this->languageManager->getLanguages();

    $language_options = [];
    foreach ($system_languages as $langcode => $language) {
      if (\in_array($langcode, $enabled_languages)) {
        $language_options[$langcode] = $language->getName();
      }
    }

    // Create an associative array with language codes.
    $availableLanguages = \implode(", ", $language_options);
    $form['description'] = [
      '#type' => 'markup',
      '#markup' => $this->t('<h3>Enabled Languages:</h3><div class="form-item"> %languages </div>', ['%languages' => $availableLanguages]),
    ];

    $form['languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Languages'),
      '#field_prefix' =>
      $this->t('<div class="form-item"><a href="#!" class="js-select-all">@select_all</a> | <a href="#!" class="js-deselect-all">@deselect_all</a></div>', [
        '@select_all' => $this->t('Select all'),
        '@deselect_all' => $this->t('Deselect all'),
      ]),
      '#options' => $language_options,
      '#required' => FALSE,
    ];

    $form['text_to_translate'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Text to translate'),
      '#description' => $this->t('Enter the English text you want to translate.'),
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
      '#weight' => 2,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Get translations'),
      '#button_type' => 'primary',
    ];

    // Check if the form state contains translations.
    if ($form_state->has('translations')) {
      // Retrieve the translations from form state.
      $translations = $form_state->get('translations');

      // Create a form element for each translation and add it
      // to the end of the form.
      $header = [
        $this->t('Language'),
        $this->t('Translation'),
        $this->t('Copy'),
      ];
      $rows = [];

      foreach ($translations as $language => $translation) {
        $row = [];
        $row[] = $language;
        $row[] = [
          'data' => [
            '#type' => 'markup',
            '#markup' => '<span id="translation_' . $language . '">' . $translation . '</span>',
          ],
        ];

        $row[] = [
          'data' => [
            '#type' => 'markup',
            '#markup' => '<a href="#!" script="javascript:void(0)" class="copy-button" data-clipboard-target="#translation_' . $language . '">' . $this->t('Copy') . '</a>',
          ],
        ];

        $rows[] = $row;
      }

      $form['translations'] = [
        '#type' => 'table',
        '#weight' => 3,
        '#header' => $header,
        '#rows' => $rows,
        '#attributes' => [
          'id' => 'translations',
        ],
      ];
    }

    $form['#attached']['library'][] = 'openai_translation/toolbox';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $text_to_translate = $form_state->getValue('text_to_translate');

    // Get the selected languages.
    $languages = \array_filter($form_state->getValue('languages'));

    // Count if $seleted_languages array is 0.
    if (\count($languages) == 0) {
      // Fetching the list of enabled languages.
      $config = $this->config('openai_translation.settings');
      $languages = $config->get('enabled_languages');
    }

    // Fetching translations for all the enabled languages.
    $translations = $this->translationService->translate($text_to_translate, $languages);

    // If the $translations array is not empty, display a message.
    if (\count($translations) > 0) {
      // Store the translations in form state to be used in buildForm() method
      // to display the translations.
      $form_state->set('translations', $translations);

      // Rebuild the form to display the translations.
      $form_state->setRebuild();
    }
    else {
      $this->messenger()->addMessage($this->t('No translations generated.'), 'error');
    }
  }

}

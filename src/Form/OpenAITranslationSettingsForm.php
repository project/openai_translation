<?php

namespace Drupal\openai_translation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * OpenAI Translation settings form.
 */
class OpenAITranslationSettingsForm extends ConfigFormBase {

  /**
   * OpenAI completion models.
   *
   * @var array
   * The GPT models available for completion.
   */
  protected array $models = [
    "GPT-3.5" => [
      "gpt-3.5-turbo" => "gpt-3.5-turbo",
      "gpt-3.5-turbo-16k" => "gpt-3.5-turbo-16k",
    ],
    "GPT-4" => [
      "gpt-4" => "gpt-4",
      "gpt-4-32k" => "gpt-4-32k",
    ],
  ];

  /**
   * The language manager.
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * Constructs a new OpenAITranslationSettingsForm object.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openai_translation_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['openai_translation.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('openai_translation.settings');

    $form['openai_api_service'] = [
      '#type' => 'select',
      '#title' => $this->t('Service Provider'),
      '#required' => TRUE,
      '#options' => [
        'openai' => $this->t('OpenAI'),
        'azure_openai' => $this->t('Microsoft Azure OpenAI'),
      ],
      '#default_value' => !empty($config->get('openai_api_service')) ? $config->get('openai_api_service') : 'openai',
    ];

    $form['openai_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key for OpenAI Chat Completions API'),
      '#default_value' => $config->get('openai_api_key'),
      '#required' => FALSE,
    ];

    $form['openai_api_org'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Organization'),
      '#default_value' => !empty($config->get('openai_api_org')) ? $config->get('openai_api_org') : '',
      '#states' => [
        'visible' => [
          ':input[name="openai_api_service"]' => ['value' => 'openai'],
        ],
      ],
    ];
    $form['openai_api_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Azure OpenAI Base URL'),
      '#default_value' => !empty($config->get('openai_api_base_url')) ? $config->get('openai_api_base_url') : '',
      '#description' => $this->t('Format example: {your-resource-name}.openai.azure.com/openai/deployments/{deployment-id}'),
      '#states' => [
        'visible' => [
          ':input[name="openai_api_service"]' => ['value' => 'azure_openai'],
        ],
      ],
    ];

    $form['openai_api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Azure OpenAI Version'),
      '#default_value' => !empty($config->get('openai_api_version')) ? $config->get('openai_api_version') : '',
      '#states' => [
        'visible' => [
          ':input[name="openai_api_service"]' => ['value' => 'azure_openai'],
        ],
      ],
    ];

    $form['openai_model'] = [
      '#type' => 'select',
      '#title' => $this->t('Model'),
      '#description' => $this->t('The model which will generate the completion. <a href="@link" target="_blank">Learn more</a>.', ['@link' => 'https://platform.openai.com/docs/models']),
      '#options' => $this->models,
      '#default_value' => !empty($config->get('openai_model')) ? $config->get('openai_model') : 'gpt-3.5-turbo',
      '#required' => TRUE,
    ];

    // Make a conditional form element $form['openai_response_format'] that is
    // only visible when $form['openai_model'] one of the GPT-4 models.
    $conditions = [];
    foreach ($this->models["GPT-4"] as $model) {
      $conditions[] = [':input[name="openai_model"]' => ['value' => $model]];
      $conditions[] = 'or';
    }
    // Remove the last 'or'.
    array_pop($conditions);

    $form['openai_response_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Response Format'),
      '#description' => $this->t('The format of the response. <a href="@link" target="_blank">Learn more</a>.', ['@link' => 'https://platform.openai.com/docs/api-reference/chat/create#chat-create-response_format']),
      '#options' => [
        'text' => $this->t('Text'),
        'json_object' => $this->t('JSON'),
      ],
      '#default_value' => !empty($config->get('openai_response_format')) ? $config->get('openai_response_format') : 'text',
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          $conditions,
        ],
      ],
    ];

    $form['enabled_languages'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled Languages for Translation'),
      '#options' => $this->getLanguageOptions(),
      '#default_value' => $config->get('enabled_languages') ?? [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $openai_model = $form_state->getValue('openai_model');
    if (!in_array($openai_model, $this->models["GPT-4"])) {
      $form_state->setValue('openai_response_format', 'text');
    }

    $this->config('openai_translation.settings')
      ->set('openai_api_service', $form_state->getValue('openai_api_service'))
      ->set('openai_api_key', $form_state->getValue('openai_api_key'))
      ->set('openai_api_base_url', $form_state->getValue('openai_api_base_url'))
      ->set('openai_api_version', $form_state->getValue('openai_api_version'))
      ->set('openai_model', $form_state->getValue('openai_model'))
      ->set('openai_response_format', $form_state->getValue('openai_response_format'))
      ->set('enabled_languages', \array_filter($form_state->getValue('enabled_languages')))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Returns an options array suitable for a checkboxes element.
   *
   * @return array
   *   An associative array of language options.
   */
  protected function getLanguageOptions(): array {
    $languages = $this->languageManager->getLanguages();

    $options = [];
    foreach ($languages as $langcode => $language) {
      $options[$langcode] = $language->getName();
    }

    return $options;
  }

}

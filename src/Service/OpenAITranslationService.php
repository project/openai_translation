<?php

namespace Drupal\openai_translation\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use OpenAI\Exceptions\ErrorException;

/**
 * Handles translation requests to the OpenAI API.
 */
class OpenAITranslationService {

  use StringTranslationTrait;

  /**
   * The HTTP client to fetch the translation.
   */
  protected ClientInterface $httpClient;

  /**
   * The config factory service.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The language manager service.
   */
  protected LanguageManagerInterface $languageManager;

  /**
   * The messenger service.
   */
  protected MessengerInterface $messenger;

  /**
   * The logger service.
   */
  protected LoggerChannelFactoryInterface $loggerFactory;

  /**
   * Constructs an OpenAITranslationService object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The logger service.
   */
  public function __construct(
      ClientInterface $http_client,
      ConfigFactoryInterface $config_factory,
      LanguageManagerInterface $language_manager,
      MessengerInterface $messenger,
      LoggerChannelFactoryInterface $loggerFactory
    ) {
    $this->httpClient = $http_client;
    $this->configFactory = $config_factory;
    $this->languageManager = $language_manager;
    $this->messenger = $messenger;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * Fetches a translation from the OpenAI API.
   *
   * @param string $text
   *   The text to translate.
   * @param array $target_languages
   *   The language to translate into.
   * @param array $options
   *   The prompt build options.
   *
   * @return array
   *   The translated text.
   */
  public function generateTranslation($text, $target_languages, $options): array {
    // Initialize the OpenAI client with the provided API key.
    $client = $this->openAiClient($options);

    $payload = [
      'model' => $options['openai_model'],
      'messages' => [
        [
          'role' => 'system',
          'content' => "
            You are a mature translator that translates English to many languages formally. You will try the best to provide translation based on the language variants and the dialects to make the translation more natural and accurate.
            The target language codes are: ```{" . \implode(',', $target_languages) . "}```. Each language name is based on ISO 639-1 standard language code.
            You will translate words, sentences or paragraphs delimited in ``` and return all translation values at once and in a clean JSON format and use the ISO639-1 language code as the keys.
            For example {\"de': \"Fahrrad\", \"fr\": \"Vélo\", \"zh-tw\" \"自行車\"}.
          ",
        ],
        [
          'role' => 'user',
          'content' => '```' . $text . '```',
        ],
      ],
      'temperature' => 0.1,
      'max_tokens' => 4000,
      'response_format' =>
        ['type' => $options['openai_response_format'] ?? 'text'],
      'top_p' => 1,
      'frequency_penalty' => 0,
      'presence_penalty' => 0,
    ];

    // Make the request to the OpenAI API.
    try {
      // Use the "openai-php/client" library to make the API call.
      $response = $client->chat()->create($payload);

      $content = $response->choices[0]->message->content ?? '';
      $data = \json_decode(\trim(\str_replace('```', '', $content)), TRUE);

      if (\is_null($data)) {
        return [];
      }

      return $data;
    }
    catch (ErrorException $e) {
      $this->loggerFactory->get('OpenAI translation toolbox')->error('Error OpenAI translation toolbox: ' . $e->getMessage());
      $this->messenger->addError($this->t('An error occurred while processing the request to OpenAI API. Please check the logs for more details.'));
      return [];
    }
    catch (RequestException $e) {
      $this->loggerFactory->get('OpenAI translation toolbox')->error('Error OpenAI translation toolbox: ' . $e->getMessage());
      $this->messenger->addError($this->t('An error occurred while processing the HTTP request. Please check the logs for more details.'));
      return [];
    }

    throw new \Exception('Translation failed. No content found in the response.');
  }

  /**
   * Set credentials.
   *
   * @param array $options
   *   The client build options.
   *
   * @return \OpenAI\Client
   *   The OpenAI client.
   */
  public function openAiClient($options): mixed {
    try {
      if ($options['openai_api_service'] == 'azure_openai') {
        $client = \OpenAI::factory()
          ->withBaseUri($options['openai_api_base_url'])
          ->withHttpHeader('api-key', $options['openai_api_key'])
          ->withQueryParam('api-version', $options['openai_api_version'])
          ->make();
      }
      else {
        $org = !empty($options['openai_api_org']) ? $options['openai_api_org'] : NULL;
        if (!empty($options['openai_api_key'])) {
          $client = \OpenAI::client($options['openai_api_key'], $org);
        }
        else {
          $this->messenger->addMessage('Missing or incorrect OpenAI API Key', 'error');
        }
      }
    }
    catch (\Exception $ex) {
      $this->messenger->addMessage($ex->getMessage(), 'error');
    }
    return $client;
  }

  /**
   * Fetches a translation from the OpenAI API.
   *
   * @param string $text
   *   The text to translate.
   * @param array $target_languages
   *   The language to translate into.
   *
   * @return array
   *   The translated text.
   */
  public function translate($text, $target_languages): array {
    $options = [];
    // Get global settings as fallback.
    $config = $this->configFactory->get('openai_translation.settings');
    $options['openai_api_service'] = $config->get('openai_api_service');
    $options['openai_api_key'] = $config->get('openai_api_key');
    $options['openai_api_base_url'] = $config->get('openai_api_base_url');
    $options['openai_api_version'] = $config->get('openai_api_version');
    $options['openai_api_org'] = $config->get('openai_api_org');
    $options['openai_model'] = $config->get('openai_model');
    $options['openai_response_format'] = $config->get('openai_response_format');

    return $this->generateTranslation($text, $target_languages, $options);
  }

}

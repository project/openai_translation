(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.openai_translation_toolbox = {
    attach: function (context, settings) {
      let clipboard = new ClipboardJS('.copy-button');

      clipboard.on('success', function (e) {
        $(e.trigger).text('Copied!');
        e.clearSelection();
      });

      $(document).on('click', function (e) {
        if ($(e.target).is('.copy-button')) {
          return;
        }
        $('.copy-button').text('Copy');
      });

      $('.js-select-all').on('click', function (e) {
        // Check all checkboxes in fieldset[data-drupal-selector="edit-languages"].
        $('fieldset[data-drupal-selector="edit-languages"] input[type="checkbox"]').prop('checked', TRUE);
      });

      $('.js-deselect-all').on('click', function (e) {
        // Uncheck all checkboxes in fieldset[data-drupal-selector="edit-languages"].
        $('fieldset[data-drupal-selector="edit-languages"] input[type="checkbox"]').prop('checked', FALSE);
      });
    }
  };

})(jQuery, Drupal, drupalSettings);

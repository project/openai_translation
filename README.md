OpenAI Translation Toolbox is a module designed for Drupal users seeking an efficient way to produce translations for their multilingual websites. It leverages the capabilities of the OpenAI platform, including Azure OpenAI, to generate accurate translations for all enabled languages on a Drupal site. With just a few clicks, users can generate, copy, and paste translations wherever needed, significantly speeding up the content creation process.

### Features

- **Bulk Translation Generation:** Easily create translations for all the available languages on your Drupal site.
- **Language Selection:** Choose which languages you want translations for from the available list.
- **Copy-Paste Efficiency:** Once translations are generated, quickly copy them and use them wherever needed on your site.
- **OpenAI Integration:** Seamlessly integrate with OpenAI's powerful translation API, including support for Azure OpenAI.

### Use Cases

For multilingual websites, the OpenAI Translation Toolbox allows users to generate translations for all their content in one go, streamlining the translation process and ensuring content consistency across different languages.

### Post-Installation

Once the module is installed, navigate to the module's configuration page. Here, you can:
- Connect to your OpenAI or Azure OpenAI account.
- Select the languages you want to enable for translation.
- Generate translations and use the copy feature to paste them in the desired locations.
